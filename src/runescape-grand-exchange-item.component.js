import HTMLShadowElement from './html-shadow-element.js';

const template = `
  <style type="text/css">
    .container { 
      display: flex; 
      flex-wrap: nowrap; 
      align-items: center; 
      padding: 1.5rem; 
    }
    .label { 
      font-weight: bold; 
      margin-right: .25rem; 
    }
    .image-container { 
      order: 1; 
      margin-right: 3rem; 
    }
    .info-container { 
      order: 2; 
      flex-basis: 100%; 
      line-height: 1.75;
    }
    .action-container { 
      order: 3; 
      opacity: .25;
      cursor: pointer;
      padding: 1rem; 
      transition: opacity .25s ease-in-out; 
    }
    .action-container:hover {
      opacity: 1;
    }
    #image { 
      width: 3rem; 
    }
  </style>

  <div class="container">
    <div class="image-container">
      <img id="image"/>
    </div>
    <div class="info-container">
      <div>
        <span class="label">Name:</span>
        <span id="name"></span>
      </div>
      <div>
        <span class="label">Price:</span>
        <span id="price"></span>
        <span>coins</span>
      </div>
      <div>
        <span class="label">Description:</span>
        <span id="description"></span>
      </div>
    </div>
    <div class="action-container">
      <span>&#10005;</span>
    </div>
  </div>
`;

export default class RunescapeGrandExchangeItem extends HTMLShadowElement {

  constructor(item, onDestroy) {
    super(template);

    this.root.querySelector('#image').setAttribute('src', item.imageUrl);
    this.root.querySelector('#name').textContent = item.name;
    this.root.querySelector('#price').textContent = item.price;
    this.root.querySelector('#description').textContent = item.description;

    this.root.querySelector('.action-container').onclick = () => onDestroy();
  }
}

customElements.define('app-runescape-grand-exchange-item', RunescapeGrandExchangeItem);
