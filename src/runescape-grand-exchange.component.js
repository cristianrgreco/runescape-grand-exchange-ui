import { debounce } from 'lodash';
import HTMLShadowElement from './html-shadow-element.js';
import RunescapeGrandExchangeItem from './runescape-grand-exchange-item.component';

const template = `
  <style type="text/css">
    :host { 
      --borderColour: #eee; 
    }
    .container { 
      margin: 0 1rem; 
      padding-bottom: .5rem; 
    }
    label { 
      margin-right: .5rem; 
    }
    #items { 
      padding-left: 0; 
    }
    #items li:first-child {
      border-top: 1px solid var(--borderColour); 
    }
    #items li { 
      list-style: none; 
      border-bottom: 1px solid var(--borderColour); 
    }
    #input {
      margin-right: 1rem;
    }
    app-spinner { 
    }
    app-error-message { 
      color: red; 
    }
    .inputText { 
      color: var(--color);
      font-size: var(--fontSize);
      font-family: var(--fontFamily);
      border: 1px solid var(--borderColour); 
      padding: .5rem .75rem; 
      border-radius: 4px; 
    }
  </style>

  <h1>Runescape Grand Exchange</h1>
  <div class="container">
    <label for="input">Enter item name:</label>
    <input type="search" id="input" class="inputText" autofocus placeholder="log, shark, feather"/>
    <app-spinner></app-spinner>
    <app-error-message message=""></app-error-message>
  </div>
  <ul id="items"></ul>
`;

export default class RunescapeGrandExchange extends HTMLShadowElement {

  constructor() {
    super(template);

    this.inputEl = this.root.querySelector('#input');
    this.spinnerEl = this.root.querySelector('app-spinner');
    this.errorMessageEl = this.root.querySelector('app-error-message');
    this.itemsEl = this.root.querySelector('#items');

    this.inputEl.oninput = debounce(() => this.onInput(), 500);
  }

  onInput() {
    const itemName = this.inputEl.value;

    if (itemName.trim().length) {
      this.fetchItem(itemName);
    }
  }

  fetchItem(itemName) {
    this.spinnerEl.show();
    this.errorMessageEl.clear();

    fetch(`http://localhost:3000/items/${itemName}`)
      .then(res => res.json())
      .then(item => {
        if (Object.keys(item).length) {
          this.addItem(item);
        } else {
          this.errorMessageEl.setAttribute('message', `Item '${itemName}' not found`);
        }
      })
      .catch(error => this.errorMessageEl.setAttribute('message', error))
      .then(() => this.spinnerEl.hide());
  }

  addItem(item) {
    const listItemEl = document.createElement('li');
    const removeListItemElFn = () => this.itemsEl.removeChild(listItemEl);
    const itemComponent = new RunescapeGrandExchangeItem(item, removeListItemElFn);
    listItemEl.appendChild(itemComponent);
    this.itemsEl.prepend(listItemEl);
  }
}

customElements.define('app-runescape-grand-exchange', RunescapeGrandExchange);
