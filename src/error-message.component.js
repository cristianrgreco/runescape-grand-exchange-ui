import HTMLShadowElement from './html-shadow-element.js';

const template = `
  <span id="message"></span>
`;

export default class ErrorMessage extends HTMLShadowElement {

  constructor() {
    super(template);

    this.messageEl = this.root.querySelector('#message');
  }

  static get observedAttributes() {
    return ['message'];
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    if (attr === 'message') {
      this.messageEl.textContent = newValue;
    }
  }

  clear() {
    this.messageEl.textContent = '';
  }
}

customElements.define('app-error-message', ErrorMessage);
