import HTMLShadowElement from './html-shadow-element.js';

const template = `
  <style type="text/css">
    #spinner { 
      color: grey; 
      letter-spacing: .15rem;
    }
    .hidden {
      display: none;
    }
  </style>

  <span id="spinner" class="hidden"></span>
`;

export default class Spinner extends HTMLShadowElement {

  constructor() {
    super(template);

    this.spinnerEl = this.root.querySelector('#spinner');
  }

  show() {
    this.spinnerEl.textContent = '';
    this.interval = setInterval(() => this.tick(), 250);
    this.spinnerEl.classList.remove('hidden');
  }

  hide() {
    window.clearInterval(this.interval);
    this.spinnerEl.classList.add('hidden');
  }

  tick() {
    const currentDotCount = this.spinnerEl.textContent.length;

    if (currentDotCount < 3) {
      this.spinnerEl.textContent += '.';
    } else {
      this.spinnerEl.textContent = '';
    }
  }
}

customElements.define('app-spinner', Spinner);
