export default class HTMLShadowElement extends HTMLElement {

  constructor(template) {
    super();

    this.root = this.attachShadow({mode: 'open'});
    this.root.innerHTML = template;
  }
}
